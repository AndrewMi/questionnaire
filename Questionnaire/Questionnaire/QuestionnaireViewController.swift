//
//  BaseSurveyViewController .swift
//  InstallerApp
//
//  Created by Andrei Mirzac on 19/08/2016.
//  Copyright © 2016 savewatersavemoney. All rights reserved.


import UIKit
import QuartzCore

class BaseSurveyViewController: UITableViewController {
	
	//MARK:Private
	private var questViewModel: QuestionnaireViewModel
	private var quest: Questionnaire
	private  enum CellRow: Int {
		case Question = 0
		case Answer = 1
	}

	init(quest: Questionnaire) {
		self.quest = quest
		self.questViewModel = QuestionnaireViewModel(quest: quest)
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		self.tableView.estimatedRowHeight = 55
		self.tableView.rowHeight = UITableViewAutomaticDimension
		
		var nib = UINib(nibName: CellIdentifier.Question.rawValue, bundle: nil)
		tableView.registerNib(nib, forCellReuseIdentifier: CellIdentifier.Question.rawValue)
		
		nib = UINib(nibName: CellIdentifier.TextAnswer.rawValue, bundle: nil)
		tableView.registerNib(nib, forCellReuseIdentifier: CellIdentifier.TextAnswer.rawValue)
	}
}

//MARK: TableView Delegate
extension BaseSurveyViewController {
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let cellRow = CellRow.init(rawValue: indexPath.row) ?? .Answer
		
		switch cellRow {
		case .Answer:
			self.handleTapOnAnswerCell(indexPath)
		case .Question:break
		}
	}
}

//MARK: TableView DataSource
extension BaseSurveyViewController {
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return questViewModel.showedQuestions.count
	}
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let question = questViewModel.showedQuestions[section]
		guard let _ = question.selectedAnswer else {
			return question.answers.count + 1
		}
		return 1
	}
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		let cellRow = CellRow.init(rawValue: indexPath.row) ?? .Answer
		
		switch cellRow {
		case .Question:
			return questionCellAtIndexPath(indexPath)
		case .Answer:
			return answerCellAtIndexPath(indexPath)
		}
	}
}

extension BaseSurveyViewController {
	func questionCellAtIndexPath(indexPath: NSIndexPath) -> UITableViewCell {
		let identifier = CellIdentifier.Question.rawValue
		let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! QuestionCell
		let question = questViewModel[indexPath.section]
		cell.configure(question: question)
		return cell
	}
	func answerCellAtIndexPath(indexPath: NSIndexPath) -> UITableViewCell {
		let identifier = CellIdentifier.TextAnswer.rawValue
		let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! TextAnswerCell
		let question = questViewModel[indexPath.section]
		let answer = question.answers[indexPath.row - 1]
		cell.configure(answer)
		return cell
	}
}

//MARK:Answer Action Handler
extension BaseSurveyViewController {
	
	func handleTapOnAnswerCell(indexPath: NSIndexPath) {
		self.questViewModel.selected(indexPath)
		self.colapse(indexPath.section, animated: true)
		if let newSection = self.questViewModel.insertNextQuestionBasedOn(indexPath) {
			self.insertSection(newSection, animated: true)
		}
	}
}

//MARK:Cell Animations
extension BaseSurveyViewController {
	func colapse(section: Int, animated: Bool) {
		let animation: UITableViewRowAnimation = animated ? .Fade : .None
		let nrRows = tableView.numberOfRowsInSection(section)
		var indexPaths = [NSIndexPath]()
		for row in 1...nrRows - 1 {
			let indexPath = NSIndexPath(forRow: row, inSection: section)
			indexPaths.append(indexPath)
		}
		tableView.beginUpdates()
		tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: section)], withRowAnimation: animation)
		tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: animation)
		tableView.endUpdates()
	}
	func insertSection(section: Int, animated: Bool) {
		let animation: UITableViewRowAnimation = animated ? .Fade : .None
		tableView.beginUpdates()
		tableView.insertSections(NSIndexSet(index: section), withRowAnimation: animation)
		tableView.endUpdates()
	}
	func updateSection(section: Int, animated: Bool) {
		let animation: UITableViewRowAnimation = animated ? .Fade : .None
		tableView.beginUpdates()
		tableView.reloadSections(NSIndexSet(index: section), withRowAnimation: animation)
		tableView.endUpdates()
	}
}
